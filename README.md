
## ex 2 

## requirements
PHP >8.0
Mysql 8.0
composer

## instalation
1. clone repository
2. configure DATABASE_URL
3. create database
4. run `composer install`
5. run `php bin/console d:s:u -f`
6. run server `symfony serve`
