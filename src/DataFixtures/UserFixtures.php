<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use App\Entity\User;

class UserFixtures extends Fixture implements ContainerAwareInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $encoder = $this->container->get('security.password_encoder');

        foreach (range(0, 19) as $i) {
            $user = new User();
            $user->setUsername('admin'.$i);
            $user->setEmail('admin'.$i.'@pg.com');
            $user->setActive(true);

            $this->addReference('user-'.$i, $user);
            $manager->persist($user);
        }

        $user = new User();
        $user->setUsername('admin');
        $user->setEmail('pg@pg.com');
        $user->setActive(true);
        $manager->persist($user);
        $manager->flush();
    }
}
